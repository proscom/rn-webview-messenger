import {
  CallMethodMessage,
  CallResultMessage,
  CallSender, isNativeMessage,
  LogFn,
  MessageTypes,
  NativeHandshakeMessage,
  NativeMessage,
  ReadyState,
  WebHandshakeMessage
} from '@proscom/rn-webview-messenger-common';
import { HandshakeTimeoutError } from './errors';

const noop = () => {};

export interface ReactNativeMessengerProps {
  window?: any;
  debug?: boolean;
  defaultCallTimeout?: number;
  handshakeTimeout?: number;
  callbackName?: string;
  webVersion?: string;
  webInfo?: any;
  log?: LogFn;
}

export class ReactNativeMessenger {
  handshakeTimeout: number;
  window: any;
  debug = false;

  isActive = false;

  callbackName: string = undefined;
  _ready = new ReadyState();
  readyTimeoutId: any = null;

  nativeInfo: NativeHandshakeMessage;

  webVersion: string = null;
  webInfo: any = null;
  webMessengerVersion: string = '0.2.0';

  sender: CallSender;

  log: LogFn;

  constructor({
    window,
    debug,
    defaultCallTimeout,
    handshakeTimeout = 30000,
    callbackName,
    webVersion,
    webInfo,
    log
  }: ReactNativeMessengerProps) {
    this.window = window;
    this.debug = debug;
    this.handshakeTimeout = handshakeTimeout;
    this.log = log || noop;
    this.callbackName = callbackName;
    this.webVersion = webVersion;
    this.webInfo = webInfo;

    this.isActive = !!window.ReactNativeWebView?.postMessage;
    if (this.debug) {
      this.log('ReactNativeMessenger.isActive =', this.isActive);
    }

    if (this.isActive) {
      this.sender = new CallSender({
        defaultCallTimeout,
        sendMessage: (message: CallMethodMessage) =>
          this.sendMethodCall(message)
      });

      this.readyTimeoutId = setTimeout(() => {
        const error = new HandshakeTimeoutError(
          'ReactNativeMessenger. Timed out waiting for handshake message from native side'
        );
        this.log(error.toString())
        this._ready.setError(error);
      }, this.handshakeTimeout);

      this.sendWebHandshake();
    }
  }

  get ready() {
    return this._ready.getPromise();
  }

  handleNativeHandshake = (message: NativeHandshakeMessage) => {
    this.nativeInfo = message;
    this.log(`nativeInfo`, JSON.stringify(this.nativeInfo, null, 2));

    this._ready.setReady();
  };

  sendWebHandshake = () => {
    const message: WebHandshakeMessage = {
      type: MessageTypes.WEB_HANDSHAKE,
      webVersion: this.webVersion,
      webInfo: this.webInfo,
      webMessengerVersion: this.webMessengerVersion,
      defaultCallback: this.callbackName
    };

    this.send(message);
  };

  handleMessage = (messageData: object) => {
    if (!isNativeMessage(messageData)) return;
    if (this.debug) {
      this.log('received message', messageData);
    }
    if (messageData.type === MessageTypes.METHOD_RESULT) {
      this.sender.receiveResult(messageData as CallResultMessage);
    } else if (messageData.type === MessageTypes.NATIVE_HANDSHAKE) {
      this.handleNativeHandshake(messageData as NativeHandshakeMessage);
    }
  };

  send = (message: NativeMessage) => {
    if (!this.window.ReactNativeWebView?.postMessage) {
      throw new Error(
        'ReactNativeMessenger. Cannot send message - not in the native context'
      );
    }
    if (this.debug) {
      this.log('sending message', message);
    }
    this.window.ReactNativeWebView.postMessage(JSON.stringify(message));
  };

  sendMethodCall = (message: CallMethodMessage) => {
    this.send({
      ...message,
      callback: this.callbackName
    } as CallMethodMessage);
  };

  call = async <Args extends any[]>(methodName: string, ...args: Args) => {
    if (!this.sender) {
      throw new Error(
        'ReactNativeMessenger. Cannot call method - not in the native context'
      );
    }

    await this._ready.getPromise();
    return this.sender.call(methodName, ...args);
  };
}
