import {
  CallMethodFn,
  CallMethodMessage,
  CallReceiver,
  isNativeMessage,
  LogFn,
  MessageTypes,
  NativeHandshakeMessage,
  NativeMessage,
  ReadyState,
  tryParseJson,
  WebHandshakeMessage
} from '@proscom/rn-webview-messenger-common';
import { Platform } from 'react-native';
import { DestroyedError } from './errors';

export type InjectJavaScriptFn = (code: string) => void;

export interface WebViewMessengerProps {
  callMethod: CallMethodFn;
  injectJavaScript: InjectJavaScriptFn;
  appVersion: string;
  info?: any;
  handshakeTimeout?: number;
  debug?: boolean;
  log?: LogFn;
}

export class WebViewMessenger {
  appVersion?: string;
  appPlatform = Platform.OS;
  nativeMessengerVersion = '0.2.0';
  info?: any;

  webInfo?: WebHandshakeMessage = null;
  _ready = new ReadyState();
  readyTimeoutId: any;

  receiver: CallReceiver;
  injectJavaScript?: InjectJavaScriptFn;

  debug?: boolean;
  log: LogFn;

  constructor({
    callMethod,
    injectJavaScript,
    appVersion,
    info,
    debug = false,
    log = () => {}
  }: WebViewMessengerProps) {
    this.appVersion = appVersion;
    this.info = info;
    this.injectJavaScript = injectJavaScript;
    this.log = log;
    this.debug = debug;
    this.receiver = new CallReceiver({
      callMethod,
      sendResponse: this.sendCallResponse
    });
  }

  destroy() {
    clearTimeout(this.readyTimeoutId);
    this._ready.setError(new DestroyedError('WebViewMessenger. Got destroyed'));
  }

  get ready() {
    return this._ready.getPromise();
  }

  update({
    callMethod,
    injectJavaScript
  }: {
    callMethod: CallMethodFn;
    injectJavaScript: InjectJavaScriptFn;
  }) {
    this.injectJavaScript = injectJavaScript;
    this.receiver.callMethod = callMethod;
  }

  getWebviewProps() {
    return {
      onMessage: this.handleWebviewMessage
    };
  }

  handleWebviewMessage = (event) => {
    const data = event.nativeEvent.data;
    const message = tryParseJson(data);
    if (isNativeMessage(message)) {
      if (this.debug) {
        this.log('Received message:', message);
      }
      if (message.type === MessageTypes.CALL_METHOD) {
        this.receiver.receiveCall(message as CallMethodMessage);
      } else if (message.type === MessageTypes.WEB_HANDSHAKE) {
        this.handleWebHandshake(message as WebHandshakeMessage);
      }
    }
  };

  handleWebHandshake = (message: WebHandshakeMessage) => {
    this.webInfo = message;
    this.log('webInfo', this.webInfo);
    clearTimeout(this.readyTimeoutId);
    this.readyTimeoutId = setTimeout(() => {
      this._ready.setReady();
    }, 0);

    const handshake: NativeHandshakeMessage = {
      type: MessageTypes.NATIVE_HANDSHAKE,
      appVersion: this.appVersion,
      appPlatform: this.appPlatform,
      nativeMessengerVersion: this.nativeMessengerVersion,
      info: this.info
    };
    this.sendCallResponse(handshake, message);
  };

  sendCallResponse = (message: NativeMessage, callMessage) => {
    if (this.debug) {
      this.log('Sending message:', message);
    }
    const callback = callMessage?.callback || this.webInfo?.defaultCallback;
    if (callback) {
      this.injectJavaScript(`
        ${callback}(${JSON.stringify(message)});
        true;
      `);
    }
  };
}
