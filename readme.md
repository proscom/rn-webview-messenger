# React Native Webview Messenger

Эта библиотека предназначена для ситуации, в которой react-native приложение
открывает в вебвью веб-приложение, и требуется общение между ними.

Например, можно открыть мобильную версию сайта в вебвью и насытить её
нативным функционалом - использованием async storage, камеры смартфона и т.п.,
а затем запаковать это в мобильное приложение.

Общение организовывается в стиле вызова удаленных процедур (RPC).
Под капотом используются функции react-native-webview для передачи сообщений:
`injectJavascript` для передачи сообщений в веб и
`window.ReactNativeWebView.postMessage` для передачи сообщений в натив.

## Установка и использование

### React Native проект

Необходимо установить пакет `@proscom/rn-webview-messenger-native` в
react-native проекте:

```
yarn add @proscom/rn-webview-messenger-native
```

В компоненте, где рендерится WebView настройте ReactNativeMessenger,
связав его с WebView и реализовав методы, которые можно будет вызывать
из веб-приложения:

```jsx harmony
import { useWebViewMessenger } from '@proscom/rn-webview-messenger-native';
import { version as appVersion } from 'package.json';

// Дополнительные параметры WebViewMessenger
const messengerConfig = {
  // Передайте, чтобы логировать важные сообщения
  log: console.log,
  // Передайте вместе с log, чтобы логировать все сообщения
  debug: true
};

function MyComponent() {
  const webviewRef = useRef(undefined);

  const callMethod = useCallback((methodName, args) => {
    // Когда веб-приложение вызывает какой-то метод из натива
    // в конечном итоге вызывается эта функция и ей передается:
    // methodName (имя вызываемого метода) и args (аргументы).
    // Поэтому здесь надо реализовать все доступные методы
    // Результат вызова можно передать обратно в веб,
    // просто вернув его из этой функции с помощью return.
    // Можно возвращать промисы
    // Для удобства можно завести отдельный объект и написать что-то вроде
    // myFuncs[methodName]?.(...args)
  }, []);

  const [messenger, getWebviewProps] = useWebViewMessenger({
    callMethod: handleRemoteCall,
    appVersion,
    webviewRef,
    // Опционально можно передать дополнительные параметры
    // конструктора WebViewMessenger.
    // Желательно при этом убедиться, что объект параметров
    // не пересоздается при каждом рендере
    messengerConfig
  });

  // messenger можно использовать для получения информации о веб-приложении
  // Например, messenger.webInfo.webVersion

  return <WebView ref={webviewRef} {...getWebviewProps()} />;
}
```

### Веб-проект

Необходимо установить пакет `@proscom/rn-webview-messenger-web` в
веб-проекте:

```
yarn add @proscom/rn-webview-messenger-web
```

Затем при старте приложения вызовите следующий код:

```js
import { ReactNativeMessenger } from '@proscom/rn-webview-messenger-web';
import { version as appVersion } from 'package.json';

export const rnMessenger = new ReactNativeMessenger({
  // Обязательно:
  // Ссылка на window
  window,
  // По этому имени будет вызываться функция приема сообщения из натива
  callbackName: 'window.rnMessenger.handleMessage',
  // Версия веб-приложения на случай если понадобится определять
  // доступность тех или иных функций
  webVersion: appVersion,

  // Опционально:
  webInfo: {}, // объект в неизменном виде передается в натив
  debug: true, // можно передать для отладки
  log: console.log // можно передать для отладки
});

// Чтобы все работало, после создания rnMessenger
// мы его сохраняем в window в соответствии с callbackName
window.rnMessenger = rnMessenger;
```

Теперь можно вызывать методы, определенные в react-native.
Перед вызовом надо проверять, открыт ли сайт в контексте react-native
приложения с помощью `rnMessenger.isActive`:

```js
if (rnMessenger.isActive) {
  rnMessenger
    .call('openCamera', arg1, arg2)
    .then((result) => {
      // ..
    })
    .catch((error) => {
      // ...
    });
}
```

Определение методов в веб и вызов из их из react-native пока не реализовано.
